# spicerack-dev-env

Ansible role that configures a development environment for [Spicerack and Cookbooks](https://wikitech.wikimedia.org/wiki/Spicerack/Cookbooks), devops tools that operate Wikimedia's tech stack. Spicerack and Cookbooks require SRE-level access to execute.

## Why do this?

To provide a low-risk way to iterate on cookbooks or spicerack itself.
It can touch production services (icinga, etc) but does not affect
the spicerack or cookbook repos as deployed in production.

## How to use (local)
- Install ansible on your desktop.
- Clone this repository.
- Change to this repository's base directory and run the following:
`ansible-playbook -i hosts spicerack-dev-env.yml`

When this is complete, you will have a dev environment on the cumin host.
Note that the [web proxy](https://wikitech.wikimedia.org/wiki/HTTP_proxy)
is [not automatically enabled via .bashrc](https://gitlab.wikimedia.org/repos/search-platform/sre/spicerack-dev-env/-/blob/main/spicerack-dev-env/tasks/main.yml#L21). You will have to enable it to build spicerack (see below).

## Suggested workflow

1. Make your changes locally (as in, not on the cumin host).
2. Submit changes as a gerrit patch.
3. From the cumin host, download your patchset with `git review -d`.
4. Enable the virtualenv with `source ~/wmf/spicerack/.tox/py39-tests/bin/activate`.
5. Enable the the [web proxy](https://wikitech.wikimedia.org/wiki/HTTP_proxy),
this is needed for building spicerack.
6. Build spicerack (which now includes your changes) by running `python setup.py install`
7. Disable the web proxy. Example command to disable web proxy: `for n in https_proxy HTTPS_PROXY HTTP_PROXY http_proxy; do unset ${n}; done`
8. Test your changes by running your newly-built spicerack. Example command: ` sudo -E ~/wmf/spicerack/.tox/py39-tests/bin/cookbook -d  -c ~/config.yaml sre.elasticsearch.rolling-operation relforge  "relforge testing" --nodes-per-run 1 --restart --task-id T301955`
9. Make more changes in response to the output. Re-enable web proxy as needed. GOTO 1.
